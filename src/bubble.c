/*
  XBubble - bubble.c

  Copyright (C) 2002  Ivan Djelic <ivan@savannah.gnu.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "utils.h"
#include "frame.h"
#include "sprite.h"
#include "setting.h"
#include "board.h"
#include "bubble.h"

extern int scale;

Bubble new_bubble( int color, double x, double y, int layer ) {
  Bubble bubble = ( Bubble ) xmalloc( sizeof( struct _Bubble ));
  bubble->state = NEW;
  bubble->cell = 0;
  bubble->clock = 0;
  bubble->color = color;
  bubble->x = x;
  bubble->y = y;
  bubble->vx = bubble->vy = 0;
  bubble->sprite = new_sprite_bubble( layer, 30, color, NEW);
  set_sprite_position( bubble->sprite, scale_x(x, scale), scale_y(y, scale) );
  return bubble;
}

void delete_bubble( Bubble bubble ) {
  delete_sprite( bubble->sprite );
  free( bubble );
}

void set_bubble_state( Bubble bubble, enum BubbleState state, int layer, 
		       int clock ) {
  Sprite s;
  s = bubble->sprite;
  bubble->state = state;
  bubble->clock = clock;   
  set_sprite_bubble_animation( s, bubble->color, state);
  set_sprite_layer( s, layer);
}

void set_bubble_position( Bubble bubble, double x, double y ) {
  bubble->x = x;
  bubble->y = y;
  set_sprite_position( bubble->sprite, scale_x(x, scale), scale_y(y, scale) );
}

int get_bubble_animation_duration( Bubble bubble ) {
  int i, t = 0;
  Set a = get_sprite_animation( bubble->sprite );
  for ( i = 0; i < a->size; i++ )
    t += ((Frame) a->element[i])->delay;
  return t;
}

int get_bubble_animation_nb_frame( Bubble bubble ) {
  Set a = get_sprite_animation( bubble->sprite );
  return a->size;
}
