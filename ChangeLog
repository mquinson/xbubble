2006-05-13   Martin Quinson (mquinson::debian.org)

	* Version 0.5.11.2
	* Let the configure use the right flags for X11R7
	
2006-03-22   Martin Quinson (mquinson::debian.org)

	* Version 0.5.11.1
	* Cleanup some code so that it compiles with gcc 4.1 too

2006-01-06   Martin Quinson (mquinson::debian.org)

	* Version 0.5.11
	* Release the current state of the code. These tiny improvements are
	  in the CVS since almost one year, that's not the way to go.
	* Take the opportunity to say that the development is now hosted on
	  alioth.debian.org (thanks, dudes)
	* Update po/Makefile.in.in
	* Allow VPATH based build (ie, make distcheck now works)

2005-04-24   Martin Quinson

	* Code cleanup, and add compiler warning/optimization flags
	* Keypress does skip animations (blinking cups and msg display)
	* Fix a buglet making the canon never stopping when fired during rotation

2003-12-09   Martin Quinson  <martin.quinson@tuxfamily.org>

        * Improve the animation of exploding board (on victory)
	* Remove useless extra checkings dedicated to the track of the bug
          apparently fixed in 0.5.10

2003-12-09   Martin Quinson  <martin.quinson@tuxfamily.org>

	* Version 0.5.10
	* Track a bug inducing that the ball against with the launched one
           bumps does not move down with the rest of the board when lowering.
	   
	  That's very strange, I'm not sure when it happens, and I'm not
           even sure it's fixed now...
	   
	  If you note its presence, I'm interested in any new info.
	  
	  The lasted hypothesis is that the launched ball moves a bit too
	    high (since the stop condition for raising is y >= target_y),
	    thus forcing some balls to be redrawn because of overlap. Then,
	    when the board is lowered, those balls are already marked as
	    modified, and thus not modified anymore. But I tried hard to
	    avoid this situation. Did I manage? Ivan, help...

2003-12-09   Martin Quinson  <martin.quinson@tuxfamily.org>

	* Version 0.5.9: "Loris and Yves go to San Diego"
	* Fix a bug inducing that malus balls were misplaced when received
          when the board have to be lowered. This was discovered during a
          long plan trip, thus the version name ;)
	* Make the frozen-bubble theme looking more like the original by not
          destroying too soon the dead balls.
	* Make the exploding balls having an initial direction and speed
          depending on the violence of the reaction (just like falling ones)

2003-11-19  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Version 0.5.8: "one step in the Debian direction without stupid bug"
        * Fix a stupid bug leading to segfault introduced in previous version

2003-11-14  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Version 0.5.7: "one step in the Debian direction"
        * Do fix the bubble overflow when the board is lowered.
	* Repackage to split the data in an architecture independent
          package, and use cdbs.
	* Demo is now between very hard computer opponents.

2003-11-10  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Cache the neighbour of each cell to speed the game even more.
	* Fixup a small display bug when all single player levels are done.

2003-10-30  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Version 0.5.6: "polishing the bubble"
	* Work on the path cache stuff.
	  - hunt bugs. There is no need anymore to have two separate tables
            depending on the number of player, which reduce the memory
            consumption and the generation time by two.
	  - generate the table at compile time
	  The games now loads about 1/3 times faster on my box.
        * Animate balls which are not launched yet.
	* Do not exchange balls when someone is dead, in order to avoid
          balls on line minus 2, which would overflow the cell array.
	* Cosmetic cleanups:
	  - mv config.h setting.h 
	  - mv xbubble_config.h config.h
	  - move the definition of color and state names from init.[ch] to
            utils.[ch] 
	  - rename functions about cell_array to follow my coding standards

2003-10-22  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Cure the parkinson syndrom from computer: 
	  do not oscillate around the aimed trajectory, and stop the canon
          when done with aiming 

2003-10-20  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Better tune the default frame per second and scale values so that
          the game keeps playable on reasonable configuration.

2003-10-19  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Version 0.5.5: "No excuse to aim so bad"
        * At least ! I found the bug in cell.c causing discrepencies in the
          tarjet computed and the one displayed !

2003-10-06  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Single player don't stop on lost game (press escape when bored).
	  That's a chance to see the last boards ;)

2003-10-01  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Add the README explaining how to design new themes, and the
          NetworkProtocol file explaining what I have in mind in that area
          in the tarballs 

2003-09-30  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Fix a memleak in place_received_bubble(). Took the oportunity to
          do a very little performance improvement here.

2003-09-30  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Version 0.5.4
        * Get rid of the FPE during graphics loading which happeared when -s
          argument was very small (leading to 0 sized graphics)
	* new graphics for dead balls by Jeremie Detrey in fancy theme

2003-09-29  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Version 0.5.3
	* Make the rotating speed of the canon increase progresively.
	  So, if you press the key once, you will rotate very few, and
           keeping the key pressed rotates quicker and quicker.
	  Should do the trick of the precise aiming.
	* Add a fail() to ease the debug of an impossible condition which
          seems to happen from time to time (in image loading phase when the
          -s is passed).
	* First work on showing the malus balls awaiting to come on the
          border of the screen. Not enabled for now, since it is not done
          yet. 

2003-09-23  Martin Quinson  <martin.quinson@tuxfamily.org>
 
        * Version 0.5.2
        * Use of a config.h for autoconf results instead of having them
          writen on the compilation line
	* Reduce the rotating speed of the cannon to allow better aiming.
          That's the best I can do for now. Reducing even more will make the
          game boring. The real problem is that the game, as it is now does
          only allow to move for the complete frame update (1/100 second),
          and not from a given amount of canon angle. Changing that needs a
          major rewrite of the game mechanism (which may be needed for the
          network game anyway)
	* Use the same formulae to bump against walls in cell.c (in
          target_cell) and in board.c (in the display mecanism).

2003-09-22  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Version 0.5.1
	* Get rid of debuging output

2003-09-22  Benjamin PATURET and friends from EPITA

        * Fix the code for precomputation cache for target_cell

        Commited by Mt: Kudos guys for fixing my bugs ;)

2003-09-22  Martin Quinson  <martin.quinson@tuxfamily.org>
        
	* Do not use -Werror for now, the code is not clean enough yet.
	* Cleanup the prototype of get_board_info() and target_cell().
	* Add a function get_color_name() for debugging
	* Tweak the size of vectors (add 10 items at once, not 100)
	* Major speed improvement: 
	  - use a cache to precompute the target_cell

2003-07-13  Martin Quinson  <martin.quinson@tuxfamily.org>

	* Only 5 malus balls can be placed at once. The others are kept for
          next time.
	* Cleanup the ball hook madness.
	  A lot of place in the board source did set need_hook_update to 1.
	  Then, at every frame animation, if this variable was set, we did
            check if some balls begin to fall.
	  But this is only needed when a launched ball reach its destination
            since no other condition may let balls unhook.
	  Moreover, this fix a little bug where a malus ball of oponent could
	    get placed on a big unhooked chunk, and fall back with the chunk.
            Now, malus ball are placed where the chunk was.
	* Malus balls are of the same color than the ones which did fall
	* Compile with -02 (damnit, it was not the case) and -Werror
	  fix the warnings
	* Cosmetic changes in function names for Vector and Set

2003-07-12  Martin Quinson  <martin.quinson@tuxfamily.org>

        Version 0.5
        * Fix two broken levels (22 and 28)
	* The direction and speed of the exploding balls is not complete
          random anymore:
 	   - For each explosion (ie a group of balls fall), a violence is
             computed, depending of the ball count.
	   - Then each ball choose randomly a direction and goes in that
             direction as quick as the explosion is violent.
          It looks really great now. Next steps are:
	   - Do not choose the direction randomly, but compute it (go away
             from explosion)
	   - Also apply these changes to the exploding ones, not only the
             unhooked ones (or even *only* to the exploding ones)
	   - reduce the effect of random in the violence (or remove it?)
	* Fix the bouncing against wall of falling balls:
	   - do bounce against ceilling
	   - bounces against walls are not elastic anymore
	
2003-05-15  Martin Quinson  <martin.quinson@tuxfamily.org>

        Version 0.4.1
        * Default FPS=50, since my computer cannot handle the fancy theme at
          100 fps. The best solution would be to have a dynamic fps, but ...
	  well... if you really want it, patches are welcome :)

2003-05-15  Ivan <ivan@savannah.gnu.org>

	* dialog.c: fixed a small background tiling bug.
	* init.c,game.c: added transparency support for board backgrounds.
	* init.c: commented out some buggy code in cleanup_graphics().

2003-05-15  Martin Quinson  <martin.quinson@tuxfamily.org>
     
        * Some memory bug fixed, thanks to valgrind
	* A match is in 3 rounds again, because it's really annoying not to
          know where the scores stand (Arnaud, do the numerical display of
          score, and a match will be in 5 rounds). 
	* Do not stop the challenge mode until the player pressed escape.
	* Better tune the way balls jump around when falling (ie, the random
          seed of their speed)

2003-05-05  Martin Quinson  <martin.quinson@tuxfamily.org>

	Version 0.4
        * Search the color of the next ball between the already existing
          ones only in single player mode
        * Do animate the malus ball until they reach their destination even
          if the game is lost.
	* Update french translation.

2003-05-05  Arnaud Legrand <arnaud.legrand@ens-lyon.fr>

        * Enhance the themming mecanism.
	* Add a frozen-bubble theme.
	* Add a config file in each theme change some settings 
	  (see data/themes/README)
	* A game is in 5 won match, with at least 2 points of difference.
	* Font color is almost configurable

2003-05-01  Ivan <ivan@savannah.gnu.org>

        * Fix the blackhole bug, which was:
            If there is a ball on the very left (resp. very right) position
	    of the second line of the board from the bottom, the collision
	    with the new ball won't be detected. The launched ball takes the
	    place of the previously placed one. The old one is still
	    displayed, but don't exist anymore for the game.

2003-04-21  Arnaud Legrand <arnaud.legrand@ens-lyon.fr>

        * Create a real themming mecanism.
        * Create a new theme from the gfx of the puzzle game. Thanks to
          Jeremie Detrey for those graphics.
	* Make the fancy theme the default one.

2003-04-21  Martin Quinson  <martin.quinson@tuxfamily.org>

        * Add a small effect to the falling balls so that they seem to jump
          in a random direction
	* In 2 players mode, player 1 is on the right when choosing handicap
          and when reporting scores
	* Pretend the balls are only 80% of their real size to relax the
          colision detection and ease the game.
	* Malus balls comes from downside and stick on existing balls. Ie,
	  they take the bottom-most position on the choosen column. 
	  Previous behaviour is still possible in the code, but you have to
          recompile the game to change the ruleset.
	* Malus balls only come after you fired. It ease the game, and must
          be for the (far away) upcoming network game.

        Known bugs:
	  - BROKEN 'RISING MALUS' VARIANTE: When activing it, the malus
            balls sometimes choose stupid locations (too deep).
	    [that's why this variant isn't activated yet]

          - UNHOOKED BALLS: Sometimes, balls keep on the board even if they
            are not hooked to the top. This arised when the malus ball
            wanted to stick on a falling group. This should be fixed, but
            seem to still happen. 
	    I forced the recomputation of hooking on several locations, but
            I really need to understand how cells works [or to rewrite it :]

2003-04-10  Martin Quinson  <martin.quinson@tuxfamily.org>

	Version 0.3
	* Bug fix: include gettext.h in the tarballs
	* UI: Enter can now be used in the menus to select, and 'q' is a
          synonym for ESC
	* board.c: New ball color is choosen only from the ones already on
	  the board 
	* levels.txt: remove some useless levels
	* config.h: ease the aiming by making the canon movement steps smaller
	* graphics/: Cleanup for future themes
	
2003-04-15  Ivan <ivan@savannah.gnu.org>

        * game.c: - Use sigsetjmp() instead of setjmp(). 
	          - Centralize critical sections calls in play_game().
	* opponent.c: bugfix, more debugging messages.
	* screens.c: alignment bugfix.

2003-04-10  Martin Quinson  <martin.quinson@tuxfamily.org>

	Version 0.2.5
	* internationalize the code and translate it to french
	* Change the level file format to the one used in fb
	* Move to autoconf 2.50

2003-04-09  gettextize  <bug-gnu-gettext@gnu.org>

	* Makefile.am (SUBDIRS): Add m4.
	(ACLOCAL_AMFLAGS): New variable.
	(EXTRA_DIST): New variable.
	* configure.in (AC_OUTPUT): Add po/Makefile.in,

2002-12-13  Ivan <ivan@savannah.gnu.org>

	Lots of code cleanup.
	Removed bogus use of CLK_TCK macro.
	* opponent.c: fixed bugs in eval_tree(), find_best_angle().
	* configure.in: version 0.2.4. Removed checks for time.h

2002-12-13  Ivan <ivan@savannah.gnu.org>

	Removed dynamic calls to XSetClipMask() which does CPU intensive 
	Bitmap->Region conversions. Sprite drawing is a LOT faster now !
	* sprite.c: use Regions for window repainting.
	* sprite.c: moved clipmask computations to initialisation phase.
	* configure.in: version 0.2.3, cleanup.
	
2002-12-13  Ivan <ivan@savannah.gnu.org>

	Added a real SINGLE_PLAYER mode support.
	Added 90 levels to SINGLE_PLAYER mode.
	Changed version number to 0.2.2.
	Moved timer handling functions in timer.c/timer.h.
	* graphics/levels.txt: contains levels in human-readable format.
	* configure.in: added "setjmp.h" in required headers.
	* cell.c: modified target_cell() so that aiming is easier.
	* game.c: wait for X server in frame_update().
	* configure.in: version 0.2.2
	
2002-12-12  Ivan <ivan@savannah.gnu.org>

	Lots of code cleanup.
	In PLAYER_VS_COMPUTER/DEMO modes, computer now "thinks" asynchronously
	during idle loop (in game.c:play_game()), instead of distributing 
	computations across game.c:frame_update() calls.

	* opponent.c: major rewrite: cleaner and much more human-readable ;-)
	* board.c, game.c: lots of code cleanup.
	* game.c: use setjmp/longjmp for async. computer thinking.
	* xbubble.c: added DEMO mode.
	* configure.in: version 0.2.1
	
2002-07-09  Ivan <ivan@savannah.gnu.org>

	* xbubble.c : merged debian bugfix for PPC arch. ( debian bug #151291 )
	* xbubble.c : changed games/rounds accounting in multi player mode.
	* cell.c : fixed NaN bug ( probably fixes debian bug #147507 ).
	* loadpng.c : fixed bug in alpha layer handling for paletted images.
	* graphics/ : added new graphics.
	* man/xbubble.6 : updated man page.
	Changed	version number to 0.2

2002-03-09  Ivan <ivan@savannah.gnu.org>

	* rgba.c, init.c: fixed memory leaks.

2002-03-08  Ivan <ivan@savannah.gnu.org>

	* xbubble.c: include time.h for RedHat/Mandrake systems.
	* configure.in: updated with AC_HEADER_TIME.

2002-03-08  Ivan  <ivan@savannah.gnu.org>
	
	* Initial release
