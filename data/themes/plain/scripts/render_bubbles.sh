#!/bin/sh

# Render bubble sprite frames using POV-Ray & the GIMP

nb_frames=10
width=480
height=360
script=explosion.pov
colors="black blue red magenta brown green yellow white"
good_frames="02 03 04 05 06 07 08 09"
bad_frames="01 10"

s=$(( $( date +%s ) + 3600 ))

# Render explosions using POV-Ray
for color in $colors; do
    cat scene.h > $script
    echo "#declare bubble_color = pigment { pigment_$color }" >> $script
    cat bubble.h >> $script
    echo "#declare random_seed = $RANDOM" >> $script
    cat explosion.h >> $script
    povray -W$width -H$height +KFF$nb_frames -GA +V -I$script +FN -O$color
done

# Convert images with the GIMP
gimp_commands="$( cat convert-bubble.scm )"

for color in $colors; do
    # use first frame as static bubble image
    file=\"$( pwd )/${color}01.png\"
    file2=\"$( pwd )/${color}.png\"
    conversion="(convert-bubble $file $file2 1)"
    gimp_commands="$gimp_commands $conversion"
    for frame in $good_frames; do
	file=\"$( pwd )/$color$frame.png\"
	conversion="(convert-bubble $file $file 0)"
	gimp_commands="$gimp_commands $conversion"
    done
done

# GIMP batch call
echo
echo
echo "Invoking the GIMP..."
echo
gimp -i -d -b "(begin $gimp_commands)" "(gimp-quit TRUE)" 2>&1 |\
{
# Make  animation text files
for color in $colors; do
    echo -n "converting $color "
    echo "# Image	file	X   Y	delay" > $color.txt
    echo "$color.png 45 45 40" >> $color.txt
    for frame in $good_frames; do
	read line
	tx=$( echo $line | sed -e 's@[^0-9]*\([0-9]\+\).*@\1@' )
	ty=$( echo $line | sed -e 's@[^0-9]*[0-9]\+[^0-9]*\([0-9]\+\).*@\1@' )
	echo "$color$frame.png $tx $ty 40" >> $color.txt
	echo -n "."
    done
    echo
done
}

# cleanup
rm -f $script
for color in $colors; do
    for frame in $bad_frames; do
	rm -f "$( pwd )/$color$frame.png"
    done
done

t=$( date -d "$s seconds ago" +"%Hh%Mmn" )
echo "Rendering + conversion took $t."
