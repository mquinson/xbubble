# rpm skeleton rewriten by Lenny Cartier <lenny@mandrakesoft.com>

%define name            xbubble
%define version         0.4
%define cvsver          20030515
%define release         1mdk
%define title           XBubble # %name with first letter capitalized.
%define longtitle       XBubble is a Bust-A-Move/Puzzle Bubble clone.

Summary:        %longtitle
Name:           %name
Version:        %cvsver
Release:        %release
URL:			http://www.nongnu.org/xbubble/
Source0:		%{name}-%{cvsver}.tar.bz2
License:		GPL
Group:          Games/Arcade # http://www.linux-mandrake.com/en/howtos/mdk-rpm/mdk-groups.html
BuildRoot:		%{_tmppath}/%{name}-buildroot
BuildRequires:  automake >= 1.7

%description
XBubble is an X Window based clone of the famous arcade game
Bust-A-Move/Puzzle Bubble. You can play it alone, against an opponent,
or even against the computer. It has nice scalable and customizable
graphics.


%prep
rm -rf $RPM_BUILD_ROOT
%setup -n %{name}

%build
./bootstrap --datadir=/usr/share
%make

%install
%makeinstall

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README
%_bindir/*
%{_datadir}/*


%changelog
* Thu May 15 2003 chl <chl@tuxfamily.org> 20030515-1mdk
- first release from cvs.
