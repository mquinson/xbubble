#ifndef _INIT_H
#define _INIT_H

void splash_screen( double zoom );
void cleanup_graphics(void);
void init_display( char * display_name );

#endif /* _INIT_H */
