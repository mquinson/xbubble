// POV-Ray script for XBubble explosion

#declare R1 = seed( random_seed )

// number of pieces = resolution ^ 3
#declare resolution = 4
// start shrinking pieces when clock >= disappear
#declare disappear = 0.5

#if ( clock <= 0) 
object { bubble }
#else
#if ( clock < disappear )
#declare anim_scale = 1
#else
#declare xx = ( clock - disappear )/( 1 - disappear )
#declare anim_scale = max( 0, 1 - xx*xx ) + .001
#end

#declare piece_size = bubble_size / resolution;
#declare piece =
object {
  box { < -0.5, -0.5, -0.5 >, < 0.5, 0.5, 0.5 > }
  scale anim_scale * piece_size
}
#declare prange = 0.5 * bubble_size - 0.5 * piece_size

// rotation amplitude
#declare rotation = 120
// pieces velocity
#declare velocity = 1.0

union {

// loop on piece location
#declare px = -prange 
#while ( px <= prange )         
#declare py = -prange 
#while ( py <= prange )
#declare pz = -prange
#while ( pz <= prange )

#declare vpiece = velocity * vnormalize( < px, py, pz > )
// add randomness in velocity length & direction
#declare perturbation = < rand(R1), rand(R1), rand(R1) > - 0.5
#declare vpiece = vpiece * ( 1 + 1.3*perturbation )
#declare perturbation = < rand(R1), rand(R1), rand(R1) > - 0.5
#declare vpiece = vrotate( vpiece, 40*perturbation )

// add randomness in rotation
#declare perturbation = < rand(R1), rand(R1), rand(R1) > - 0.5
#declare rpiece = rotation * (1 + perturbation )

// select a small piece of bubble
object {
  bubble
  clipped_by { 
    piece
    // add some randomness in piece size & orientation
    scale ( 1 + 1.6*( < rand(R1), rand(R1), rand(R1) > - 0.5 ))
    rotate 300 * ( 1 + ( < rand(R1), rand(R1), rand(R1) > - 0.5 ))
    translate < px, py, pz >
  }
  // center piece for rotation
  translate < -px, -py, -pz >
  rotate rpiece * clock
  // final translation
  translate < px, py, pz > + ( vpiece * clock )
}

#declare pz = pz + piece_size
#end
#declare py = py + piece_size
#end
#declare px = px + piece_size
#end
} // end of union
#end
