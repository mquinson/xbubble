#ifndef _TIMER_H
#define _TIMER_H

long get_closest_itimer_interval( long usec );
void start_timer( long usec, void (*handler)(int));
void stop_timer(void);
void timer_sleep( long ms );
void block_timer(void);
void unblock_timer(void);

#endif /* _TIMER_H */
