// POV-Ray header for XBubble

#declare bubble_size = 6
#declare bubble = 
object {
  sphere { <0, 0, 0>, bubble_size/2 }
  texture { 
    pigment { bubble_color }
    finish  {
      ambient 2.0
      diffuse 0.1
      reflection 0.7
      refraction 1
      ior 1.45
      specular 0.8
      roughness 0.003
      phong 1.0
      phong_size 400
    }
  }
  hollow
}

