#ifndef _CONTROLS_H
#define _CONTROLS_H

#include <X11/keysym.h>

#define PLAYER1_LEFT(k)  ((k == XK_Left) ||(k == XK_KP_4)||(k == XK_KP_Left))
#define PLAYER1_RIGHT(k) ((k == XK_Right)||(k == XK_KP_6)||(k == XK_KP_Right))
#define PLAYER1_UP(k)    ((k == XK_Up)   ||(k == XK_KP_8)||(k == XK_KP_Up))
#define PLAYER1_DOWN(k)  ((k == XK_Down) ||(k == XK_KP_2)||(k == XK_KP_Down))
#define PLAYER1_FIRE(k)  ((k == XK_Up)||(k == XK_KP_5)||(k == XK_KP_Begin))

#define PLAYER2_UP(k)    ( k == XK_d )
#define PLAYER2_DOWN(k)  ( k == XK_c )
#define PLAYER2_LEFT(k)  ( k == XK_x )
#define PLAYER2_RIGHT(k) ( k == XK_v )
#define PLAYER2_FIRE(k)  ( k == XK_c )

#endif /* _CONTROLS_H */
