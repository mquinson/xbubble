#ifndef _SCREENS_H
#define _SCREENS_H

#include "game.h"

enum ScreenMain_Choice {
  S1_ESCAPE = -1,
  S1_SINGLE_PLAYER = 0,
  S1_TWO_PLAYERS,
  S1_PLAYER_VS_COMPUTER,
  S1_CHALLENGE,
  S1_DEMO,
  S1_SETTINGS,
  S1_QUIT
};

enum ScreenComputerSkill_Choice {
  S2_ESCAPE = -1,
  S2_VERY_EASY,
  S2_EASY,
  S2_NORMAL,
  S2_HARD,
  S2_VERY_HARD
};

enum Screen2p_Choice {
  S3_CONTINUE,
  S3_END
};

enum handicap_2p_Choice{
  S4_ESCAPE = -1,
  S4_OK
};

enum handicap_1p_Choice {
  S5_ESCAPE = -1,
  S5_O,
  S5_1,
  S5_2,
  S5_3,
  S5_4
};

enum ScreenSettings_Choice {
  S6_ESCAPE = -1,
  S6_KEY,
  S6_CANON,
  S6_THEME,
  S6_BACK
};

enum ScreenMain_Choice screen_main(enum ScreenMain_Choice preset);
enum ScreenComputerSkill_Choice screen_computer_skill(void);
enum Screen2p_Choice screen_2p( enum GameMode mode, int *rounds, int *games );
enum handicap_2p_Choice screen_2p_handicap( int *choice );
enum handicap_1p_Choice screen_1p_handicap(void);

enum ScreenSettings_Choice screen_settings(enum ScreenSettings_Choice preset);
  
void screen_unimplemented(void);

#endif /* _SCREENS_H */
