#ifndef _BOARD_H
#define _BOARD_H

#include "cell.h"
#include "sprite.h"

typedef struct _Board * Board;

typedef struct {
  int malus_top;      /* true if malus balls should take the highest free pos, 
			 false if they should stick on the lowest possible pos */
  int max_fire_delay; /* in second */
  int chainreaction;  /* boolean */
  int single;         /* single player? (ie, new bubble color must be already there */
} RuleSet_t;

#define DEFAULT_RULE_SET { 1, 5, 1, 1 }

enum BoardState {
  WAITING_FOR_CANON,
  READY_TO_FIRE,
  REBOUND,
  RECEIVING_BUBBLES,
  WON,
  LOST,
  FROZEN
};

Board new_board( int period, RuleSet_t *ruleset, int mov_ceiling, int *colors );
void delete_board( Board board );
void reset_board( Board board );

void animate_board( Board board , int dt );
SpritePool get_board_sprite_pool( Board board );

void explode_board( Board board );
void freeze_board( Board board );
void get_bubble( Board board, int color ); /* other board give you malus ball that way */
int give_bubble( Board board ); /* other board get malus balls from you that way */
void recompute_malus_indicator( Board board );
int board_empty( Board board );

enum BoardState get_board_state( Board board );

void canon_rotate_left( Board board );
void canon_rotate_right( Board board );
void canon_stop( Board board );
void canon_fire( Board board );
void canon_move( Board board, int dt );
int get_canon_angle( Board board );

int board_was_lowered( Board board );

int get_last_fire_delay( Board board );
CellArray get_board_array( Board board );
void get_board_info( Board board,
		     double **vx, 
		     double **vy,
		     int *color,
		     int *next_color,
		     int *launch_count,
		     int *period );
#endif /* _BOARD_H */
