#ifndef _RECTANGLE_H
#define _RECTANGLE_H

typedef struct {
  int x1;
  int y1;
  int x2;
  int y2;
} Rectangle;

struct _RectangleList {
  Rectangle *element;
  int size;
  int max_size;
};
typedef struct _RectangleList * RectangleList;

RectangleList new_rectangle_list( int max_size );
void delete_rectangle_list( RectangleList rl );
void empty_rectangle_list( RectangleList rl );
void add_disjoint_rectangle_to_list( RectangleList rl, int, int, int, int );

#endif /* _RECTANGLE_H */

