; GIMP script-fu
; Ivan Djelic <ivan@savannah.gnu.org>
; convert-bubble converts a povray-generated animation frame to an
; XBubble sprite PNG image.

(define (convert-bubble source dest static-bubble)
  (set!	image (car (gimp-file-load TRUE source source )))
  (gimp-image-undo-disable image)
  (set! layer (car (gimp-image-flatten image )))
  (gimp-layer-add-alpha layer)
  (gimp-by-color-select layer '(0 0 0) 0 REPLACE FALSE FALSE 0 FALSE)
  (if (= static-bubble TRUE )
      (begin
	;; add a 2 pixel wide black border
	(gimp-selection-invert image)
	(gimp-selection-grow image 2)
	(gimp-selection-invert image)))
  (gimp-edit-cut layer)
  (if (= static-bubble FALSE )
      (begin 
	;; add a sparkle effect
	(set! color '(127 127 127))
	(gimp-palette-set-background color)
	(set! layer (car (gimp-image-flatten image )))
	(plug-in-sparkle TRUE image layer 0.001 0.5 20 4 -1 0.25 0 0 0 0 0 0 0)
	(gimp-layer-add-alpha layer)
	(gimp-by-color-select layer color 0 REPLACE FALSE FALSE 0 FALSE)
	(gimp-edit-cut layer)))
  (set! width 320)
  (set! height 240)
  (gimp-image-scale image width height)
  (if  (= static-bubble TRUE )
       (begin
	 (plug-in-autocrop TRUE image layer)
	 ;; ensure image size is right
	 (gimp-image-scale image 90 90)))
  (if  (= static-bubble FALSE )
       (begin
	 ;; dirty hack for computing translation required after auto-cropping
	 (set! pixel (cons-array 4 'byte))
	 (aset pixel 0 255)
	 (aset pixel 1 255)
	 (aset pixel 2 255)
	 (aset pixel 3 255)
	 ;; put a white pixel in lower right corner and auto-crop
	 (gimp-drawable-set-pixel layer (- width 1) (- height 1) 4 pixel )
	 (plug-in-autocrop TRUE image layer)
	 (set! cx (- width (car (gimp-image-width image))))
	 (set! cy (- height (car (gimp-image-height image))))
	 (set! tx (- (/ width 2) cx ))
	 (set! ty (- (/ height 2) cy ))
	 (aset pixel 0 0)
	 (aset pixel 1 0)
	 (aset pixel 2 0)
	 (aset pixel 3 0)
	 ;; erase pixel & auto-crop
	 (set! width (car (gimp-image-width image)))
	 (set! height (car (gimp-image-height image)))
	 (gimp-drawable-set-pixel layer (- width 1) (- height 1) 4 pixel )
	 (plug-in-autocrop TRUE image layer)
	 ;; convert image to indexed palette (smaller size)
	 (gimp-convert-indexed image 2 MAKE-PALETTE 255 TRUE 0 "" )
	 ;; ouput translation coordinates
	 (gimp-message (string-append (number->string tx) " " 
				      (number->string ty)))))
  (file-png-save TRUE image layer dest dest FALSE 9 0 0 0 0 0 )
  (gimp-image-delete image))