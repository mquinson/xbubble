#include "config.h"

#ifndef _SETTING_H
#define _SETTING_H

#define COLS                 8
#define ROWS                 12
#define NB_COLORS            8

/* bubble size */
#define MAX_SCALE            90
#define DEFAULT_SCALE        60
#define COLLISION_DIST       0.80 /* when looking for collision, bubble pretend to be only 80% of their real diameter */
#define TRAJ_MAX_LENGTH      71   /* count of steps on the longuest trajectories */

/* board window geometry */
#define SIDE_WIDTH           58
#define FRAME_HEIGHT         58

#define DEAD_ZONE_HEIGHT     2.0
#define BOARD_WIDTH          COLS
#define BOARD_HEIGHT         ( ROWS*ROW_HEIGHT + DEAD_ZONE_HEIGHT )
#define OFFSET_X             ( 1.0*SIDE_WIDTH/MAX_SCALE )
#define OFFSET_Y             ( 1.0*FRAME_HEIGHT/MAX_SCALE )
#define scale_x(X,S)         (( X + OFFSET_X )*(S) )
#define scale_y(Y,S)         (( Y + OFFSET_Y )*(S) )
#define board_win_width(S)   scale_x( OFFSET_X + BOARD_WIDTH, S ) 
#define board_win_height(S)  scale_y( BOARD_HEIGHT, S )
#define board_spacing(S)     (S)
#define game_win_width(S)    ((int)(2*board_win_width(S) + 3*board_spacing(S)))
#define game_win_height(S)   ((int)( board_win_height(S) + 3*board_spacing(S)))

/* sprite locations */
#define ALERT_X              ( BOARD_WIDTH*0.5 )
#define ALERT_Y              ( -OFFSET_Y*0.5 )
#define NEW_X                1.0
#define NEW_Y                ( BOARD_HEIGHT - 0.75 )
#define COUNTDOWN_X          1.3
#define COUNTDOWN_Y          ( NEW_Y - 1.3 )

/* canon */
#define CANON_X              ( BOARD_WIDTH*0.5 )
#define CANON_Y              ( ROWS*ROW_HEIGHT + 0.5 )
#define CANON_CX             220
#define CANON_CY             220
#define CANON_ANGLE_MAX      100
#define NB_ANGLES            ( 2*CANON_ANGLE_MAX+1 )
#define ANGLE_STEP           ( 1.32 / CANON_ANGLE_MAX ) /* approx. 75 deg */
#define CANON_ROTATING_SPEED ( 1/50.0 )

/* bubble speeds in cells/ms */
#define LAUNCHING_SPEED      0.02     /* velocity of launched bubbles */
#define EXPLODING_SPEED      0.0003   /* vertical velocity after explosion */
#define RISING_SPEED         0.04     /* velocity of incoming bubbles */
#define GRAVITY              0.00003  /* gravity in cell.ms^-2 */

/* times in milliseconds */
#define PROPAGATION_TIME     15       /* explosion propagation time */
#define CANON_LOAD_TIME      500
#define COUNTDOWN            5000
#define DEFAULT_HANDICAP     7000
#define SLOW_BLINKING        250      /* blinking light period */
#define FAST_BLINKING        125

/* game parameters */
#define PERIOD               12
#define COMPUTER_DELAY       1500
#define PLAYER1_TAG          N_("Player 1")
#define PLAYER2_TAG          N_("Player 2")
#define COMPUTER_TAG         N_("Computer")
#define DEMO_TAG             N_("Demo")

/* game fonts */
/* this one is from the freefont package */
#define SUPPLIED_FONT  "-freefont-baskerville-*-i-*-*-%d-*-*-*-*-*-iso8859-1"
#define FONTS {\
  "-*-curlz mt-*-*-*-*-%d-*-*-*-*-*-iso8859-1",\
  "-bitstream-charter-*-*-*-*-%d-*-*-*-*-*-*-*",\
  SUPPLIED_FONT,\
  NULL }
#define TITLEFONT_COLOR {0xff, 0xe4, 0xff}
#define MENUFONT_COLOR {0xc0, 0xc0, 0xc0}
#define MENUFONT_SELECTED_COLOR {0xc0, 0xc0, 0x00}
#define MAX_NB_LEVELS  100

/* game screens */
#define DEFAULT_FPS        80

#define MIN(a,b) ((a)<(b)? (a):(b))
#define MAX(a,b) ((a)>(b)? (a):(b))

#endif /* _SETTING_H */
