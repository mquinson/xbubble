/*
  XBubble - screens.c

  Copyright (C) 2002  Ivan Djelic <ivan@savannah.gnu.org>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>

#include "setting.h"
#include "gettext.h"

#include "rgba.h"
#include "dialog.h"
#include "screens.h"
#include "game.h"

extern Display *display;
extern Window win;
extern Pixmap win_bg;

extern int win_width;
extern int win_height;
extern XFontStruct *title_font;
extern XFontStruct *dialog_font;
extern XFontStruct *menu_font;
extern char *player_name[][2];

extern int titlefont_color[3];
extern int menufont_color[3];
extern int notitle;

enum ScreenMain_Choice screen_main(enum ScreenMain_Choice preset) {
  int title_height;
  int menu_height;
  int menu_x;
  int menu_y;
  Window title=(Window)NULL;
  const char* menu[] = {
    _("Single Player"),
    _("Two Players"),
    _("Player vs Computer"),
    _("Challenge"),
    _("Demo"),
    _("Settings"),
    _("Quit")
  };
  int choice = preset;
 
  title_height = title_font->ascent + title_font->descent;
  get_menu_size( 7, menu, &menu_x, &menu_height, 0 );
  if(!notitle)
    title = create_dialog( win_width/2, ( win_height - menu_height )/2,
			   "XBubble", title_font, 
			   get_pixel( titlefont_color[0], titlefont_color[1], 
				      titlefont_color[2] ),0, 1.0 );
  menu_x = win_width/2;
  menu_y = ( win_height + title_height )/2;
  get_menu_choices( 7, menu, &menu_x, &menu_y, &choice, 1, False, True );
  if(!notitle)
    XDestroyWindow( display, title );
  return choice;
}

enum ScreenComputerSkill_Choice screen_computer_skill() {
  int title_height;
  int menu_height;
  int menu_x;
  int menu_y;
  Window title;
  int choice = S2_NORMAL;
  const char* menu[] = {
    _("Very easy"),
    _("Easy"),
    _("Normal"),
    _("Hard"),
    _("Very hard")
  };

  title_height = dialog_font->ascent + dialog_font->descent;
  get_menu_size( 5, menu, &menu_x, &menu_height, 0 );
  title = create_dialog( win_width/2, ( win_height - menu_height )/2,
			 _("Select computer skill"),
			 dialog_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ), 0, 1.0 );
  menu_x = win_width/2;
  menu_y = ( win_height + title_height )/2;
  get_menu_choices( 5, menu, &menu_x, &menu_y, &choice, 1, False, True );
  XDestroyWindow( display, title );
  return choice;
}

enum Screen2p_Choice screen_2p( enum GameMode mode, int *rounds, int *games ) {
  int i,j;
  int title_y;
  int title_height;
  int menu_height;
  int width[3];
  int height[3];
  int menu_x;
  int menu_y;
  int stats_x[3];
  int stats_y;
  int nb_rounds;
  int percentage[2];
  int choice = S3_CONTINUE;
  unsigned long pixel[4];
  Window title;
  Window stats_box[3];
  const char* menu[] = { _("Continue"),_("End")  };
  char* pstats[3][4];
  char  stats[3][4][32] = { { "","","","" }, { "","","","" }, 
			    { " ", N_("Game Wins"), N_("Round Wins"), N_("Percentage") }};
			 
  for ( i = 0; i < 2; i++ )
    for ( j = 0; j < 4; j++ )
      pstats[i][j] = stats[i][j];
   
  for ( j = 0; j < 4; j++ )
    pstats[2][j] = gettext(stats[2][j]);
   
  pixel[0] = get_pixel( 0xff, 0xc0, 0xff );
  pixel[1] = get_pixel( 0x60, 0xe0, 0xff );
  pixel[2] = get_pixel( 0xdd, 0xdd, 0xdd );
  pixel[3] = get_pixel( 0xdd, 0xdd, 0xdd );
 
  nb_rounds = rounds[0] + rounds[1];
  percentage[0] = ( nb_rounds != 0 )? 100*rounds[0]/nb_rounds : 0 ;
  percentage[1] = ( nb_rounds != 0 )? 100 - percentage[0] : 0 ;
  for ( i = 0; i < 2; i++ ) {
    j= i?0:1;
    strcpy(pstats[i][0], gettext(player_name[mode][j]));
    sprintf( pstats[i][1], "%d", games[j] );
    sprintf( pstats[i][2], "%d", rounds[j] );
    sprintf( pstats[i][3], "%d", percentage[j] );
  }
  for ( i = 0; i < 3; i++ ) {
    get_column_size( 4, (const char**)pstats[i], menu_font, &width[i], &height[i] );
    width[i] *= 1.5;
    height[i] *= 1.5;
  }
  get_menu_size( 2, menu, &menu_x, &menu_height, 0 );
  title_height = dialog_font->ascent + dialog_font->descent;
  title_y = ( win_height - height[0] - menu_height )/2;
  stats_y = title_y + ( title_height + height[0] )/2;
  menu_x = win_width/2;
  menu_y = stats_y + ( height[0] + menu_height )/2;
  stats_x[0] = ( win_width - width[0] - width[2] )/2;
  stats_x[1] = ( win_width + width[1] + width[2] )/2;
  stats_x[2] = win_width/2;
  /* title window */
  title = create_dialog( win_width/2, title_y, _("End of game"), dialog_font,
			 get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ), 0, 1.0 );
  /* statistics windows */
  for ( i = 0; i < 3; i++ )
    stats_box[i] = create_column( stats_x[i], stats_y, 4, (const char**)pstats[i], menu_font,
				  pixel, 1.0 );
  /* continue/end menu */
  get_menu_choices( 2, menu, &menu_x, &menu_y, &choice, 1, False, True );
  /* cleanup */
  XDestroyWindow( display, title );
  for ( i = 0; i < 3; i++ )
    XDestroyWindow( display, stats_box[i] );

  return choice;
}

enum handicap_2p_Choice screen_2p_handicap( int * choice ) {
  int subtitle_y;
  int footnote_y;
  int menu_x[2];
  int menu_y[2];
  int title_height;
  int menu_width;
  int menu_height;
  int title_y;
  const char* menu[5] = { "1", "2", "3", "4", "5" };
  Window title;
  Window subtitle1;
  Window subtitle2;
  Window footnote;

  title_height = 3*( dialog_font->ascent + dialog_font->descent )/2;
  get_menu_size( 5, menu, &menu_width, &menu_height, 1 );
  title_y = ( win_height - title_height - 4*menu_height )/2;
  subtitle_y = title_y + title_height;
  menu_x[0] = win_width/4;
  menu_y[0] = subtitle_y + ( title_height + menu_height )/2;
  menu_x[1] = 3*win_width/4;
  menu_y[1] = menu_y[0];
  footnote_y = menu_y[0] + 2*menu_height;
  choice[0] = 2;
  choice[1] = 2;
  /* title windows */
  title = create_dialog( win_width/2, title_y, _("Select handicap level"), 
			 dialog_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ), 0, 1.0 );
  subtitle1 = create_dialog( menu_x[0], subtitle_y, _("Player 2"), 
			     dialog_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ),0,1.0);
  subtitle2 = create_dialog( menu_x[1], subtitle_y, _("Player 1"), 
			     dialog_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ),0,1.0);
  footnote = create_dialog( win_width/2, footnote_y, _("Press return to start"), 
			    menu_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ), 0, 1.0 );
  get_menu_choices( 5, menu, menu_x, menu_y, choice, 2, True, True );
  XDestroyWindow( display, title );
  XDestroyWindow( display, subtitle1 );
  XDestroyWindow( display, subtitle2 );
  XDestroyWindow( display, footnote );
  return (( choice[0] < 0 )? S4_ESCAPE : S4_OK );
}

enum handicap_1p_Choice screen_1p_handicap() {
  int choice;
  int subtitle_y;
  int footnote_y;
  int menu_x;
  int menu_y;
  int title_height;
  int menu_width;
  int menu_height;
  int title_y;
  Window title;
  Window subtitle;
  Window footnote;
  const char* menu[5] = { "1", "2", "3", "4", "5" };

  title_height = 3*( dialog_font->ascent + dialog_font->descent )/2;
  get_menu_size( 5, menu, &menu_width, &menu_height, 1);
  title_y = ( win_height - title_height - 4*menu_height )/2;
  subtitle_y = title_y + title_height;
  menu_x = win_width/2;
  menu_y = subtitle_y + ( title_height + menu_height )/2;
  footnote_y = menu_y + 2*menu_height;
  choice = 2;
  /* title windows */
  title = create_dialog( win_width/2, title_y, _("Select handicap level"),
			 dialog_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ), 0, 1.0 );
  subtitle = create_dialog( menu_x, subtitle_y, _("Player 1"),
			    dialog_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ),0,1.0);
  footnote = create_dialog( win_width/2, footnote_y, _("Press fire to start"),
			    menu_font, get_pixel( menufont_color[0], menufont_color[1], 
				    menufont_color[2] ), 0, 1.0 );
  get_menu_choices( 5, menu, &menu_x, &menu_y, &choice, 1, True, True );
  XDestroyWindow( display, title );
  XDestroyWindow( display, subtitle );
  XDestroyWindow( display, footnote );
  return choice;
}

enum ScreenSettings_Choice screen_settings(enum ScreenSettings_Choice preset) {
  int title_height;
  int menu_height;
  int menu_x;
  int menu_y;
  Window title=(Window)NULL;
  const char* menu[] = {
    _("Key controls"),
    _("Canon sensibility"),
    _("Theme"),
    _("Back")
  };
  int choice = preset;
 
  title_height = title_font->ascent + title_font->descent;
  get_menu_size( 4, menu, &menu_x, &menu_height, 0 );
  if(!notitle)
    title = create_dialog( win_width/2, ( win_height - menu_height )/2,
			   _("Settings"), title_font, 
			   get_pixel( titlefont_color[0], titlefont_color[1], 
				      titlefont_color[2] ),0, 1.0 );
  menu_x = win_width/2;
  menu_y = ( win_height + title_height )/2;
  get_menu_choices( 4, menu, &menu_x, &menu_y, &choice, 1, False, True );
  if(!notitle)
    XDestroyWindow( display, title );
  return choice;
}

#include <unistd.h>
extern Window root;
extern GC dialog_gc;

void screen_unimplemented() {
  int menu_height;
  int menu_x;
  int menu_y;
  Window title=(Window)NULL;
  const char* menu[] = {  _("Not implemented") };
  int choice = 0;
 
  get_menu_size( 1, menu, &menu_x, &menu_height, 0 );
  menu_x = win_width/2;
  menu_y = win_height/2;
  get_menu_choices( 1, menu, &menu_x, &menu_y, &choice, 1, False, True );
}
