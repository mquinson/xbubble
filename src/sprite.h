#ifndef _SPRITE_H
#define _SPRITE_H
#include <X11/Xlib.h>

#include "utils.h"
#include "setting.h"

typedef struct _Sprite * Sprite;
typedef struct _SpritePool * SpritePool;

Sprite new_sprite( int layer, int overlapmax, Set a, int frame, int cycle );
void delete_sprite(Sprite s);

void set_sprite_position( Sprite s, int x, int y );
void set_sprite_animation( Sprite s, Set a, int frame, int cycle );
Set get_sprite_animation( Sprite s );

Sprite new_sprite_bubble( int layer, int overlap_max, int color, int state );
void set_sprite_bubble_animation( Sprite s, int color, int state);

void set_sprite_layer( Sprite s, int layer );
void set_sprite_frame( Sprite s, int frame );
void increase_sprite_frame( Sprite s, int increase );
void increase_sprite_clock( Sprite s, int dt );

SpritePool new_sprite_pool( int nb_layers, int nb_sprites_max );
void delete_sprite_pool( SpritePool sp );

void add_sprite_to_pool( SpritePool sp, Sprite s );
void remove_sprite_from_pool( SpritePool sp, Sprite s );

void draw_sprite_pool( SpritePool sp, Pixmap dest, Pixmap bg, int w, int h );
void redraw_sprite_pool( SpritePool sp, Pixmap pixmap, Pixmap background, int forceful );
GC get_sprite_pool_redraw_gc( SpritePool sp );

#endif /* _SPRITE_H */
