#ifndef _OPPONENT_H
#define _OPPONENT_H

#include "board.h"

typedef struct _Opponent * Opponent;

enum Level {
  VERY_EASY,
  EASY,
  NORMAL,
  HARD,
  VERY_HARD
};

Opponent new_opponent( Board board, int level );
void delete_opponent( Opponent opponent );
int find_best_angle( Opponent op, int *best_eval );

#endif /* _OPPONENT_H */

