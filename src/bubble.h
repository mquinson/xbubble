#ifndef _BUBBLE_H
#define _BUBBLE_H

#include "sprite.h"

struct _Bubble {
  enum BubbleState state;
  int color;
  int clock;
  int cell;
  double x;
  double y;
  double vx;
  double vy;
  double target_y;
  Sprite sprite;
};
typedef struct _Bubble * Bubble;

Bubble new_bubble( int color, double x, double y, int layer );
void delete_bubble( Bubble bubble );
void set_bubble_state( Bubble bubble,enum BubbleState state, int layer, int c);
void set_bubble_position( Bubble bubble, double x, double y );
int get_bubble_animation_duration( Bubble bubble );
int get_bubble_animation_nb_frame( Bubble bubble );

#endif /* _BUBBLE_H */

