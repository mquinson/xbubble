#ifndef _GAME_H
#define _GAME_H

#include "board.h"
#include "opponent.h"

enum GameMode { 
  SINGLE_PLAYER,
  TWO_PLAYERS,
  PLAYER_VS_COMPUTER,
  DEMO
};

enum GameResult {
  DRAW,
  PLAYER1_WON,
  PLAYER1_LOST,
  ABORTED
};

typedef struct _Game * Game;

Game new_game( enum GameMode mode,
	       RuleSet_t *ruleset,
	       int *colors,
	       int round,
	       int *score,
	       enum Level level );

void delete_game( Game game, int no_clean );

enum GameResult play_game( Game game );

#endif /* _GAME_H */



