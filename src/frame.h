#ifndef _FRAME_H
#define _FRAME_H

#include <X11/Xlib.h>

typedef struct _Frame {
  int width;
  int height;
  int cx;
  int cy;
  int delay;
  int has_mask;
  Pixmap pixmap;
  GC gc;
} * Frame;

#endif /* _FRAME_H */
