#ifndef _RGBA_H
#define _RGBA_H

#include <X11/Xlib.h>

struct _RgbaImage {
  unsigned char *data;
  int width;
  int height;
  int hotx;
  int hoty;
  int has_alpha;
};
typedef struct _RgbaImage * RgbaImage;

RgbaImage new_rgba_image( int width, int height );
void delete_rgba_image( RgbaImage ri );

RgbaImage create_rgba_image_from_png_file( const char *filename );
RgbaImage copy_rgba_image( RgbaImage ri );
RgbaImage scale_rgba_image( RgbaImage ri, double scalex, double scaley );
RgbaImage crop_rgba_image( RgbaImage ri, int x1_arg, int y1_arg, int x2_arg, int y2_arg );
RgbaImage rotate_rgba_image( RgbaImage ri, double angle, int autocrop );

void create_pixmaps_from_rgba_image( RgbaImage ri, Pixmap *rgb, Pixmap *mask);
unsigned long get_pixel( int red, int green, int blue );

#endif /* _RGBA_H */
