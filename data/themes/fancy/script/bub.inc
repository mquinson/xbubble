#include "colors.inc"

camera {  
  angle 10
  location <0, 0, -16>
  look_at <0, 0, 0>
}

light_source { <-4, 4, -4> color White }
background { color rgb <0.7, 0.7, 0.7> }

#declare bub =
  object {
    difference {
      sphere { <0, 0, 0>, 1 }
      sphere { <0, 0, 0>, 0.9 }
    }

    finish {
      specular 1
      roughness 0.003
      ambient 0.1
      diffuse 1
      refraction 1 
      reflection 0.05
      ior 1
    }
  }

#declare rd = -4*clock;
#declare rd2 = 1.5 + 2*clock;
#declare bubex =
  #if (clock < 0)
  intersection {
    blob {
      threshold 0.5
      #declare cnt = 0;
      #while (cnt < 8)
        sphere { <rd2, 0, 0>, rd, 1 rotate (45*cnt)*y }
        #declare cnt = cnt + 1;
      #end
      #declare cnt = 0;
      #while (cnt < 4)
        sphere { <rd2, 0, 0>, rd, 1 rotate 45*z rotate (22.5*90*cnt)*y }
        sphere { <rd2, 0, 0>, rd, 1 rotate -45*z rotate (22.5+90*cnt)*y }
        #declare cnt = cnt + 1;
      #end
      sphere { <0, rd2, 0>, rd, 1 }
      sphere { <0, -rd2, 0>, rd, 1 }
    }
    difference {
      sphere { <0, 0, 0>, rd2 }
      sphere { <0, 0, 0>, rd2-0.1 }
    }

    rotate 60*x
    rotate 30*z

    finish {
      specular 1
      roughness 0.003
      ambient 0.1
      diffuse 1
      refraction 1 
      reflection 0.2
      ior 1
    }
  }
  #else
  sphere { <0, 0, 0>, 0 }
  #end

#declare p_black = pigment { color rgbf <0.3, 0.3, 0.4, 0.8> }
#declare p_blue = pigment { color rgbf <0, 0, 1, 0.8> }
#declare p_green = pigment { color rgbf <0, 1, 0, 0.8> }
#declare p_orange = pigment { color rgbf <1, 0.8, 0, 0.8> }
#declare p_purple = pigment { color rgbf <1, 0, 1, 0.8> }
#declare p_red = pigment { color rgbf <1, 0, 0, 0.8> }
#declare p_white = pigment { color rgbf <1, 1, 1, 0.8> }
#declare p_yellow = pigment { color rgbf <1, 1, 0, 0.8> }

#declare rd = 0.6;
#declare sr = rd * sqrt(1/2);
#declare o_black =
  object {
    cylinder { <0, -sr, 0>, <0, sr, 0>, sr }

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*180*z
    rotate clock*360*x
  }

#declare rd = 0.6;
#declare sh = 0.1;
#declare sr = sqrt(rd*rd - sh*sh);
#declare tp = sin(pi/5) / (1 + cos(pi/5));
#declare sr2 = sr*tp*sqrt(1+tan(pi/5)*tan(pi/5)) / (tp + tan(pi/5));
#declare o_blue =
  object {
    prism {
      linear_sweep linear_spline -sh, sh, 11,
      <sr, 0>,
      <cos(pi/5)*sr2, sin(pi/5)*sr2>, <cos(2*pi/5)*sr, sin(2*pi/5)*sr>,
      <cos(3*pi/5)*sr2, sin(3*pi/5)*sr2>, <cos(4*pi/5)*sr, sin(4*pi/5)*sr>,
      <-sr2, 0>,
      <cos(4*pi/5)*sr, -sin(4*pi/5)*sr>, <cos(3*pi/5)*sr2, -sin(3*pi/5)*sr2>,
      <cos(2*pi/5)*sr, -sin(2*pi/5)*sr>, <cos(pi/5)*sr2, -sin(pi/5)*sr2>,
      <sr, 0>
    }

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*72*y
    rotate clock*360*x
  }

#declare rd = 0.6;
#declare sc = rd * pow(1/3, 1/3);
#declare o_green =
  box {
    <-sc, -sc, -sc>, <sc, sc, sc>

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*90*y
    rotate clock*360*x
  }

#declare rd = 0.6;
#declare sh = 0.1;
#declare sr = sqrt(rd*rd - sh*sh);
#declare o_orange =
  object {
    difference {
      cylinder { <0, -sh, 0>, <0, sh, 0>, sr }
      cylinder { <sr/2, -2*sh, 0>, <sr/2, 2*sh, 0>, sr }
    }

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*360*y
    rotate clock*180*x
  }

#declare rd = 0.6;
#declare sh = 0.1;
#declare sr = sqrt(rd*rd - sh*sh) / 2;
#declare eps = 0.01;
#declare sr2 = sqrt((sr-eps)*(sr-eps) + 8*sr*sr);
#declare sx = ((3*sr*sr + sr*sr2) / sr2 - eps) / (1 + (sr*sr)/(sr2/sr2));
#declare sy = sqrt(sr2*sr2 - sx*sx);
#declare o_purple =
  object {
    union {
      cylinder { <sr-eps, -sh, -sr>, <sr-eps, sh, -sr>, sr }
      cylinder { <-sr+eps, -sh, -sr>, <-sr+eps, sh, -sr>, sr }
      prism {
        linear_sweep linear_spline -sh, sh, 6,
        <0, 2*sr>, <-sx, 2*sr-sy>, <-2*sr+eps, -sr>,
        <2*sr-eps, -sr>, <sx, 2*sr-sy>, <0, 2*sr>
      }
    }

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*180*z
    rotate clock*360*x
  }

#declare rd = 0.6;
#declare sh = rd / 3;
#declare sr = sqrt(rd*rd - sh*sh);
#declare o_red =
  object {
    prism {
      conic_sweep linear_spline 0, 1, 4,
      <sr, 0>, <cos(2*pi/3)*sr, sin(2*pi/3)*sr>,
      <cos(2*pi/3)*sr, -sin(2*pi/3)*sr>, <sr, 0>
      scale <1, rd+sh, 1>
      translate <0, -rd, 0>
    }

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*120*y
    rotate clock*360*x
  }

#declare rd = 0.6;
#declare sr = rd * sqrt(1/2);
#declare o_white =
  object {
    cone { <0, -sr, 0>, sr, <0, sr, 0>, 0 }

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*360*z
    rotate clock*360*x
  }

#declare rd = 0.6;
#declare sx = 2*rd/3 * cos(clock*pi);
#declare sy = rd/3 * sin(clock*pi);
#declare o_yellow =
  object {
    blob {
      threshold 0.5
      sphere { <sx, sy, 0>, rd, 1 }
      sphere { <-sx, -sy, 0>, rd, 1 }
    }

    finish { ambient 0.5 }
    pigment { White }
    rotate clock*180*y
  }
