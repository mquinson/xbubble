#ifndef _DIALOG_H
#define _DIALOG_H

#include <X11/Xlib.h>

void get_column_size( int nb_lines,
		      const char ** line,
		      XFontStruct *xfont,
		      int *width,
		      int *height );

Window create_column( int x,
		      int y,
		      int nb_lines,
		      const char ** line, 
		      XFontStruct *xfont,
		      unsigned long *pixel,
		      double expand );

Window create_dialog( int x,
		      int y,
		      const char * msg,
		      XFontStruct *xfont, 
		      unsigned long pixel,
		      int border_width,
		      double expand );

void get_menu_size( int nb_entries,
		    const char ** entries,
		    int *width,
		    int *height,
		    int horizontal );

void get_menu_choices( int nb_entries,
		       const char ** entries,
		       int *x,
		       int *y, 
		       int *selection,
		       int nb_menus,
		       int horizontal,
		       int escapable );

#endif /* _DIALOG_H */
