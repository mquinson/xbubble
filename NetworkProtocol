This is the protocol xbubble will use to play in network mode.

Some assumtion first:
  - Every player as a connexion to every other one (in a clique manner).
  - There is no cheater. So, every player has the Truth over his own board.
    If there is some difference between the locally computed version of
      another player and the board he publish, the second version is the
      right one.
    Rational: detecting cheaters is very hard, and reducing the network
      requierements seems more important to us.
  - No packet loss, Packet order preserved (thanks TCP)

The requirements:
  - Game rules and gameplay unmodified
  - Playable on a machine able to play current version
  - Latency up to 5-10 seconds, bandwidth smaller than 2k
  - If possible, it must look great, with the board of all opponents, and
    the malus balls flying from one board to the other.

In order to achieve this contradictory requirements, this is a multi-level
protocol, where the lower level is absolutely required to play with the high
one is only to make this look great. 

The protocol is designed so that any message of level N only need messages
of levels lower to N. So, if a client A decides to restrict itself to a low
level, it will only affect the displaying of the other player's board by
him, and of A's on other player's computer. The game is still valid, and
other remote displays will not be affected.

LEVEL 1: Absolute requirements for the game to work
========

GOTYOU: tell the target (and no other) that it just received N malus balls.
  No loss, no cheater => no need for ACK
  format: "GOTYOU <date> <# balls>" 6 bits (1+4+1). The date is not needed
   under level 3.
  
RIP: Broadcast the senders death.
  format: "RIP" 1 bit.


Those two messages are enough to let the game run in dumb mode (ie, without
seeing other player's board) when there is no cheaters.


RELAX: Control message over the protocol. The sender asks the receiver to
  stop using a protocol level higher to the argument (but the receiver is
  free to restrict itself to a yet lower level if himself suffers to follow
  the beat). This is not a broadcast, since the player may have more
  interest in some board than in some others (its current target -- see
  LOCKED -- may be cruxial for example).

  format: "RELAX <maximal accepted level>" 2 bits.



Niveau 2: Minimal display of remote boards (one frame every N sec)
========

SNAP: broadcast every N secondes of the current board.
  format: "SNAP <date> <8*12 colors=96> <# malus ball waiting>" ~100 bits.
  You must wait that there is no flying ball to publish a SNAP.
  
  The frequency of this message is still to be determined. If the level 3 is
    used, 30 seconds should be enough since SNAP is then used to
    resyncronize the remote computation to the local Truth. (but if every one
    follows level 3, there will be no desyncronisation. There will be some
    for the players at level 3 if someone goes under L3, therefor not
    disclosing some information).
  
  On the receiver side, when a SNAP is received, it is always accepted even
    if the L3 computation don't match (in that case, a warning is issued. We
    suppose there is no cheater, but we are not stupid).
  
LOCKED: broadcast of the player when he changes its target (ie, the receiver
  of the malus ball he produces). Yes, you can choose your target.
  format: "LOCKED <date> <# of player>" 6b.

  This message is used at level 3 to compute where the malus balls should go,
    but at level 2, it is simply used to display a green box around the
    board of the player you are currently targeting, and a red one around
    the one of people currently targeting you.
  
Niveau 3: Real-time display of remote boards (N frames per second)
========

GOTHIM: Tell the other players that the sender emited a GOTYOU
  format: "GOTHIM <date> <who> <# balls>" 7b

FIRE: broadcast the fire done
  format: "FIRE <date> <angle> <color to reload the canon>" 7b
  
OUCH: broadcast where and when the malus balls were placed
  format "OUCH <date> <# balls> <col x color list>" less than 16b since
    malus balls are placed 5 after 5,

After a FIRE, the possible GOTYOU and GOTHIM messages are sent immediatly,
their date being in the future at that time and corresponding to the time at
which the malus balls will be produced. They are ready for consumtion on the
target side one second after production, to let the graphical interface show
them flying. The rational to send those message in the past is to overlap
communication latency with visual ball fly.


Each host using L3 keeps a local copy of each other hosts board. Along to
this, it keeps a list of the recently received messages (ie all ones since
the last SNAP). That way, it is possible to replay the events since last
SNAP when needed (like a LOCKED + GOTHIM from a remote host A concerning
another remote host B comming *to us* after the FIRE of B, but received *by
B* before this FIRE).

Note that if you emit the GOTHIM before the FIRE, it may suppress the only
possibility of message interleaving changing the content of a board, and
simplify the implementation. This still has to be demonstrated though.


Niveau 4: Useless but great looking stuff
========

POS: broadcast cannon position change (may represent quite a high amount of
     messages)
  format: "POS <date> <angle>" 2b.


We belive that this protocol may permit to play even accross RTC modem, with
bandwidth needs being around 50b/s. And another great possible use of this
protocol is to save games on disk to replay them afterward...


Here you are, you now know how you should implement this stuff, since we
have currently no time for that :)
