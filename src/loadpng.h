#ifndef _LOADPNG_H
#define _LOADPNG_H

unsigned char * load_png_file( const char *filename, 
			       int *width,
			       int *height,
			       int *has_alpha );
#endif /* _LOADPNG_H */
