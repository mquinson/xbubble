// POV-Ray header for XBubble

#include "colors.inc"

#declare pigment_dead =    pigment { color rgbf <0.2, 0.2, 0.2, 0.9> }
#declare pigment_black =   pigment { color rgbf <0.5, 0.5, 0.5, 0.9> }
#declare pigment_blue =    pigment { color rgbf <  0, 0.5,   1, 0.9> }
#declare pigment_magenta = pigment { color rgbf <  1,   0,   1, 0.9> }
#declare pigment_green =   pigment { color rgbf <  0,   1, 0.2, 0.9> }
#declare pigment_red =     pigment { color rgbf <  1,   0,   0, 0.9> }
#declare pigment_yellow =  pigment { color rgbf <  1,   1,   0, 0.9> }
#declare pigment_brown =   pigment { color rgbf <  1, 0.5,   0, 0.9> }
#declare pigment_white =   pigment { color rgbf <  1,   1,   1, 0.9> }

global_settings { assumed_gamma 1.3 ambient_light 1.0 }

camera {
  location < 0, 0, -16.8 >
  look_at  < 0, 0, 0 >
}

light_source { <4, 5, -5> color White }
